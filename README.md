<p align="center"><b>HARIAK EMULATOR</b></p>
<p align="center">
The clean, stable and robust Habbo Emulator! 
</p>
<p align="center">
PRODUCTION-201702211601-24705679
</p>

## About Hariak

Hariak is a Habbo Emulation software that emulates the private usage of Habbo Hotels. It is created with clean, robust code and contains good quality code. Along with its great code base, Hariak is constantly updated on the latest PRODUCTION build along with using the latest up-to-date C# coding standards and librarys.

Hariak is accessible, yet powerful, providing an emulator that is clean, stable and full of features ready to use. It also comes with a superb combination of simplicity, elegance, and innovation that makes your life even easier when using it.

## License

Hariak Emulator is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT).
